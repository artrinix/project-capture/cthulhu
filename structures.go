package main

import (
	"github.com/golang-jwt/jwt"
	"time"
)

type Media struct {
	ID         uint   `gorm:"primaryKey;autoIncrement"`
	Key        string `gorm:"index"`
	Data       []byte
	Filename   string
	Mimetype   string
	Filetype   string
	Size       uint
	UploadedAt time.Time `gorm:"autoCreateTime"`
	Author     string
}

type User struct {
	ID              uint `gorm:"primaryKey;autoIncrement"`
	Username        string
	Email           string
	Admin           bool
	Password        string
	AccountCreation time.Time `gorm:"autoCreateTime"`
}

type jwtClaims struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Admin    bool   `json:"admin"`
	jwt.StandardClaims
}
