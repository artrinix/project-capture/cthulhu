package main

import (
	"bytes"
	"fmt"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
	"io"
	"net/http"
	"strings"
)

func uploadMedia(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*jwtClaims)
	username := claims.Username

	file, err := c.FormFile("file")
	if err != nil {
		e.Logger.Error(err)
	}

	src, err := file.Open()
	if err != nil {
		e.Logger.Error(err)
	}

	buf := bytes.NewBuffer(nil)

	if _, err := io.Copy(buf, src); err != nil {
		e.Logger.Error(err)
	}

	if err := src.Close(); err != nil {
		e.Logger.Error(err)
	}

	contentType := file.Header.Get("Content-Type")

	media := Media{
		Key:      RandomString(20),
		Data:     buf.Bytes(),
		Filename: file.Filename,
		Mimetype: contentType,
		Filetype: strings.Split(contentType, "/")[1],
		Size:     uint(file.Size),
		Author:   username,
	}

	// If they key already exists somehow, instead of causing duplicates or errors, just generate another random key
	// There can't be two random keys that are the same... right?
	var existing Media
	if result := db.Where(&Media{Key: media.Key}).Find(&existing); result.RowsAffected > 0 {
		media.Key = RandomString(20)
	}

	if err := db.Create(&media).Error; err != nil {
		e.Logger.Error(err)
	}

	return c.JSON(http.StatusOK, echo.Map{
		"key":       media.Key,
		"author":    username,
		"type":      contentType,
		"deleteUrl": fmt.Sprintf("%s/api/delete/%s", viper.GetString("URL"), media.Key),
		"imageUrl":  fmt.Sprintf("%s/%s", viper.GetString("URL"), media.Key),
	})
}

func deleteMedia(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*jwtClaims)
	username := claims.Username

	var media Media
	if result := db.Where(&Media{Key: c.Param("id")}).First(&media); result.Error != nil {
		return echo.NewHTTPError(http.StatusNotFound, "Could not find any media with that key.")
	}

	if media.Author == username {
		db.Delete(&media)
		return c.JSON(http.StatusOK, echo.Map{"message": fmt.Sprintf("Deleted %s.", media.Key)})
	} else {
		return echo.NewHTTPError(http.StatusUnauthorized, "You don't have permission to delete this media.")
	}

}
