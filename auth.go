package main

import (
	"errors"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
	"gorm.io/gorm"
	"net/http"
	"time"
)

func createAccount(c echo.Context) error {
	user := User{
		Username:        c.FormValue("username"),
		Email:           c.FormValue("email"),
		Admin:           false,
		Password:        HashAndSalt([]byte(c.FormValue("password"))),
		AccountCreation: time.Time{},
	}

	result := db.Create(&user)

	if result.Error != nil {
		e.Logger.Error(result.Error)
		return echo.NewHTTPError(http.StatusInternalServerError, "Failed to create the user")
	}

	return c.JSON(http.StatusOK, echo.Map{
		"id":    user.ID,
		"email": user.Email,
	})
}

func loginAccount(c echo.Context) error {
	var user User

	email := c.FormValue("email")
	password := c.FormValue("password")

	result := db.Where(&User{Email: email}).First(&user)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		e.Logger.Error(result.Error)
	}

	if ComparePasswords(user.Password, []byte(password)) == false {
		return echo.NewHTTPError(http.StatusUnauthorized, "Invalid user, check the email and password")
	}

	claims := &jwtClaims{
		user.Username,
		user.Email,
		user.Admin,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 8760).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	t, err := token.SignedString([]byte(viper.GetString("SECRET")))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, echo.Map{
		"token": t,
	})
}
