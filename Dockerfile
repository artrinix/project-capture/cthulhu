FROM gcr.io/distroless/static

COPY ./cthulhu_release /
CMD [ "/Cthulhu" ]
