package main

import (
	"bytes"
	"embed"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"math/rand"
	"net/http"
	"time"
)

//go:embed "public"
var public embed.FS

var seededRand *rand.Rand

var db *gorm.DB
var e *echo.Echo

func main() {
	seededRand = rand.New(rand.NewSource(time.Now().UnixNano()))

	initConfigs()

	initDatabase()

	initEcho()

	setupRoutes()

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", viper.GetInt("PORT"))))
}

func initConfigs() {
	// Set default ENV settings and values
	viper.SetEnvPrefix("CTHULHU")

	// Cthulhu Keys
	viper.SetDefault("PORT", 7201)
	viper.SetDefault("SECRET", "iamverystupid")            // The secret used to encode tokens, I recommend changing this
	viper.SetDefault("URL", "localhost")                   // Cthulhu URL without trailing forward-slash, You'll need to include the port if it isn't 80 or 443
	viper.SetDefault("REQUIRE_EXTENSION_FOR_IMAGE", false) // Allows having no extension for images, still required for other media.
	viper.SetDefault("CHARSET", "abcdefghijklmnopqrstuvwxyz"+"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	// Database Keys
	viper.SetDefault("DB_HOST", "localhost")
	viper.SetDefault("DB_PORT", 5432)
	viper.SetDefault("DB_USER", "cthulhu")
	viper.SetDefault("DB_PASSWORD", "iamsmort")
	viper.SetDefault("DB_NAME", "cthulhu")
	viper.SetDefault("DB_SSL", "disable")
	viper.SetDefault("DB_TIMEZONE", "Etc/UTC")

	// Load config.yaml
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; we ignore the error because we fall back on default env
		} else {
			// Config file was found but another error was
			panic(err)
		}
	}

	// Override config file with ENV variables
	viper.AutomaticEnv()
}

func initEcho() {
	// Create Echo
	e = echo.New()
	// Add the Logger middleware
	e.Use(middleware.Logger())
	// Add the Recover middleware to handle panics and other bullshit
	e.Use(middleware.Recover())
}

func initDatabase() {
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s TimeZone=%s",
		viper.GetString("DB_HOST"),
		viper.GetInt("DB_PORT"),
		viper.GetString("DB_USER"),
		viper.GetString("DB_PASSWORD"),
		viper.GetString("DB_NAME"),
		viper.GetString("DB_SSL"),
		viper.GetString("DB_TIMEZONE"),
	)

	var err error

	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		e.Logger.Fatal(err)
	}

	// Migrate Tables
	if err := db.AutoMigrate(&Media{}, &User{}); err != nil {
		e.Logger.Fatal(err)
	}
}

func setupRoutes() {
	e.StaticFS("/", public)

	// Base routes
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	e.GET("/:id", func(c echo.Context) error {
		var media Media
		db.Where(&Media{Key: c.Param("id")}).First(&media)

		buf := bytes.NewBuffer(media.Data)

		return c.Stream(http.StatusOK, media.Mimetype, buf)
	})

	// Auth Routes
	auth := e.Group("/auth")

	auth.POST("/create", createAccount)
	auth.POST("/login", loginAccount)

	// API Routes
	api := e.Group("/api")

	api.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		Claims:      &jwtClaims{},
		SigningKey:  []byte(viper.GetString("SECRET")),
		TokenLookup: "header:Authorization",
		ErrorHandlerWithContext: func(err error, c echo.Context) error {
			return echo.NewHTTPError(http.StatusUnauthorized, "you are not in the sudoers file. This incident will be reported.")
		},
	}))

	api.POST("/upload", uploadMedia)
	api.DELETE("/delete/:id", deleteMedia)
}

func HashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		e.Logger.Error(err)
	}

	return string(hash)
}

func ComparePasswords(hashedPwd string, plainPwd []byte) bool {
	byteHash := []byte(hashedPwd)

	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		e.Logger.Error(err)
		return false
	}

	return true
}

func RandomString(length int) string {
	charset := viper.GetString("CHARSET")

	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}
